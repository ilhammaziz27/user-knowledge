/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

/**
 *
 * @author ilhammaziz
 */
public class Trapezoidal {
    private double p1;
    private double p2;
    private double p3;
    private double p4;
    private String label;
    
    public Trapezoidal(String label,double p1,double p2,double p3,double p4){
        this.p1=p1;
        this.p2=p2;
        this.p3=p3;
        this.p4=p4;
        this.label=label;
    }
    
    public double getMembershipVal(double val){
        return val>=p1 && val<=p2?p2==0?1
                :(val-p1)/(p2-p1)
                :val>=p2 && val<=p3?1
                :val>=p3 && val<=p4?(-1)*(val-p4)/(p4-p3)
                :0;
    }

    public double getP1() {
        return p1;
    }

    public void setP1(double p1) {
        this.p1 = p1;
    }

    public double getP2() {
        return p2;
    }

    public void setP2(double p2) {
        this.p2 = p2;
    }

    public double getP3() {
        return p3;
    }

    public void setP3(double p3) {
        this.p3 = p3;
    }

    public double getP4() {
        return p4;
    }

    public void setP4(double p4) {
        this.p4 = p4;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
