/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

/**
 *
 * @author ilhammaziz
 */
public class FuzzyRule {
    public Membership getMembership(Attribute att){
        Membership selected=att.getMembershipVals().get(0);
        
        for(Membership mv:att.getMembershipVals()){
            if(selected.getValue()<=mv.getValue()){
                selected=mv;
            }
        }
        
        return selected;
    }
    
    public String getOutputResult(User obj){
        Membership mv1=getMembership(obj.getAttributes().get(0));
        Membership mv2=getMembership(obj.getAttributes().get(1));
        Membership mv3=getMembership(obj.getAttributes().get(2));
        
        //Sepertinya ini akan sangat melelahkan
        switch (mv1.getLabel()){
            case "Bad":
                switch (mv2.getLabel()){
                    case "Bad":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "very_low";
                            case "Poor":
                                return "Low";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Poor":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Low";
                            case "Poor":
                                return "Low";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Fair" :
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Low";
                            case "Poor":
                                //return "Low";
                                return "Middle";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Good":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "very_low";
                            case "Poor":
                                return "Middle";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    default:
                        return "Undefined";
                }
            case "Poor":
                switch (mv2.getLabel()){
                    case "Bad":
                        switch (mv3.getLabel()){
                            case "Bad":
                                //return "Low";
                                return "very_low";
                            case "Poor":
                                return "Low";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                break;
                        }
                        break;
                    case "Poor":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Low";
                            case "Poor":
                                return "Low";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Fair" :
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Low";
                            case "Poor":
                                return "Middle";
                            case "Fair" :
                                //return "High";
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Good":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Middle";
                            case "Poor":
                                return "High";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    default:
                        return "Undefined";
                }
                break;
            case "Fair" :
                switch (mv2.getLabel()){
                    case "Bad":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "very_low";
                            case "Poor":
                                return "Low";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                break;
                        }
                        break;
                    case "Poor":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Low";
                            case "Poor":
                                return "Low";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Fair" :
                        switch (mv3.getLabel()){
                            case "Bad":
                                //return "Low";
                                return "Middle";
                            case "Poor":
                                return "Middle";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Good":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Low";
                            case "Poor":
                                return "Middle";
                            case "Fair" :
                                return "High";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    default:
                        break;
                }
                break;
            case "Good":
                switch (mv2.getLabel()){
                    case "Bad":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "very_low";
                            case "Poor":
                                return "Low";
                            case "Fair" :
                                return "High";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Poor":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Low";
                            case "Poor":
                                return "Middle";
                            case "Fair" :
                                //return "High";
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Fair" :
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Middle";
                            case "Poor":
                                return "Middle";
                            case "Fair" :
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    case "Good":
                        switch (mv3.getLabel()){
                            case "Bad":
                                return "Low";
                            case "Poor":
                                return "Middle";
                            case "Fair" :
                                //return "High";
                                return "Middle";
                            case "Good":
                                return "High";
                            default:
                                return "Undefined";
                        }
                    default:
                        return "Undefined";
                }
            default:
                return "Undefined";
        }
        return "Undefined";
    }
}
