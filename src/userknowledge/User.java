/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author ilhammaziz
 */
public class User {
    private String label;
    private ArrayList<Attribute> attributes;
    private String outputVal;

    public User(String label,String outputVal){
        this.label=label;
        this.attributes=new ArrayList<>();
        this.outputVal=outputVal;
    }
    
    public void printMemberhsipVal(){
        for(Attribute att:getAttributes()){
            for(Membership mv:att.getMembershipVals()){
                DecimalFormat df = new DecimalFormat("#.##"); 
                System.out.printf(mv.getLabel()+"("+df.format(mv.getValue())+")"+"  ");           
            }
            System.out.println("");
        }
    }
    
    public void addAttributeVal(double val){
        Attribute att=new Attribute();
        att.setInputVal(val);
        this.getAttributes().add(att);
    }
    
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getOutputVal() {
        return outputVal;
    }

    public void setOutputVal(String outputVal) {
        this.outputVal = outputVal;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }
}
