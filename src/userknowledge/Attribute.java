/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

import java.util.ArrayList;

/**
 *
 * @author ilhammaziz
 */
public class Attribute{
    private ArrayList<Membership> membershipVals;
    private double inputVal;
    private String label;
    
    public Attribute(){
        this.membershipVals=new ArrayList<>();
    }
    
    public void printMembershipVal(){
        
    }

    public double getInputVal() {
        return inputVal;
    }

    public void setInputVal(double inputVal) {
        this.inputVal = inputVal;
    }

    public ArrayList<Membership> getMembershipVals() {
        return membershipVals;
    }

    public void setMembershipVals(ArrayList<Membership> membershipVals) {
        this.membershipVals = membershipVals;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
