/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

/**
 *
 * @author ilhammaziz
 */
public class Driver {
    //Sopir
    public static void main(String[] args) {
        MembershipFunction stg=new MembershipFunction();
        stg.addMembershipModel("Bad", 0, 0, 0.15, 0.25);
        stg.addMembershipModel("Poor", 0.25, 0.35, 0.35, 0.5);
        stg.addMembershipModel("Fair", 0.5, 0.6, 0.6, 0.75);
        stg.addMembershipModel("Good", 0.75, 0.85, 1, 1);
        
        MembershipFunction scg=new MembershipFunction();
        scg.addMembershipModel("Bad", 0, 0, 0.15, 0.25);
        scg.addMembershipModel("Poor", 0.25, 0.35, 0.35, 0.5);
        scg.addMembershipModel("Fair", 0.5, 0.6, 0.6, 0.75);
        scg.addMembershipModel("Good", 0.75, 0.85, 1, 1);
        
        MembershipFunction peg=new MembershipFunction();
        peg.addMembershipModel("Bad", 0, 0, 0.15, 0.25);
        peg.addMembershipModel("Poor", 0.25, 0.35, 0.35, 0.5);
        peg.addMembershipModel("Fair", 0.5, 0.6, 0.6, 0.75);
        peg.addMembershipModel("Good", 0.75, 0.85, 1, 1);
        
        Application uk=new Application();
        uk.importDataset("//home//ilhammaziz//Dataset UNS.xlsx");
        uk.addMembershipFunction(stg);
        uk.addMembershipFunction(scg);
        uk.addMembershipFunction(peg);
        
        uk.calculateMembershipVal();
        
        FuzzyRule fr=new FuzzyRule();
        //Wahai program, jalanlah!
        uk.getResult(fr);
    }
}
