/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

/**
 *
 * @author ilhammaziz
 */
public class Membership {
    private String label;
    private double value;
    
    public Membership(String label, double value){
        this.label=label;
        this.value=value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
