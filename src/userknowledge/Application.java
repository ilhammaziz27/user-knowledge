/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

/**
 *
 * @author ilhammaziz
 */
public class Application {
    private ArrayList<MembershipFunction> mfs;
    private ArrayList<User> dataset;
    
    public Application(){
        this.mfs=new ArrayList<>();
        this.dataset=new ArrayList<>();
    }
    
    public void getResult(FuzzyRule fr){
        int i=0;
        int dataTrue=0;
        
        System.out.printf("No\tAttr-1\tAttr-2\tAttr-3\tTarget\tOutput\n");
        for(User usr:dataset){
            System.out.printf(++i+"\t"+
                    usr.getAttributes().get(0).getInputVal()+"\t"+
                    usr.getAttributes().get(1).getInputVal()+"\t"+
                    usr.getAttributes().get(2).getInputVal()+"\t"+
                    usr.getOutputVal()+"\t"+
                    fr.getOutputResult(usr)+"\n"
            );
            fr.getOutputResult(usr);
            
            if(usr.getOutputVal().equals(fr.getOutputResult(usr)))
                dataTrue++;
        }
        
        System.out.println("");
        System.out.println("Valid Data\t:"+i);
        System.out.println("Total Data\t:"+dataTrue);
        System.out.println("Accuracy\t:"+(dataTrue*100/i)+"%");
    }
    
    public void addMembershipFunction(MembershipFunction mf){
        this.mfs.add(mf);
    }
    
    public void calculateMembershipVal(){
        for(User usr:getDataset()){
            int i=0;
            for(MembershipFunction mf:mfs){
                double val=usr.getAttributes().get(i).getInputVal();
                usr.getAttributes().get(i).setMembershipVals(mf.getMembershipVal(val));
                i++;
            }
        }
    }

    public void importDataset(String url){
        ExcelUtil eu=new ExcelUtil(url);
        XSSFSheet sheet=eu.getExcelSheet();
        
        ArrayList<User> objects=new ArrayList<>();
        
        boolean firstRow=true;
        Iterator<Row> rows = sheet.iterator();
        while(rows.hasNext()){
            Row row = rows.next();
            
            if(firstRow){
                firstRow=false;
                continue;
            }
            
            String label=String.valueOf(row.getCell(0).getNumericCellValue());
            String outputVal=row.getCell(row.getLastCellNum()-1).getStringCellValue();
            User obj=new User(label, outputVal);
            
            int i=1;
            while(i!=row.getLastCellNum()-1){
                obj.addAttributeVal(row.getCell(i).getNumericCellValue());
                i++;
            }
            
            objects.add(obj);
        }
        this.dataset = objects;
    }

    public ArrayList<User> getDataset() {
        return dataset;
    }
}
