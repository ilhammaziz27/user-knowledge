/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

import java.util.ArrayList;

/**
 *
 * @author ilhammaziz
 */
public class MembershipFunction {
    private ArrayList<Trapezoidal> models;
    
    public MembershipFunction(){
        this.models=new ArrayList<>();
    }
    
    public void addMembershipModel(String label,double p1,double p2,double p3,double p4){
        Trapezoidal tm=new Trapezoidal(label, p1, p2, p3, p4);
        this.models.add(tm);
    }

    public ArrayList<Membership> getMembershipVal(double val) {
        ArrayList<Membership> mfs=new ArrayList<>();
        for(Trapezoidal mf:models){
            Membership mv=new Membership(mf.getLabel(), mf.getMembershipVal(val));
            mfs.add(mv);
        }
        
        return mfs;
    }
    
}
