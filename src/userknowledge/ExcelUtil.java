/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userknowledge;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ilhammaziz
 */
public class ExcelUtil {
    private String fileName;
    
    public ExcelUtil(String fn){
        this.fileName=fn;
    }
    
    public XSSFSheet getExcelSheet(){        
        try{
            FileInputStream fis = new FileInputStream(this.getFileName());
            
            XSSFWorkbook workBook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workBook.getSheetAt(0);
            System.out.println("Excel worksheet loaded!");
            return sheet;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            //ex.printStackTrace();
            System.out.println("Excel worksheet load failed!");
            return null;
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
